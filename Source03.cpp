#include <iostream>

using namespace std;

class Base {
public:
	void showBase() {
		cout << "Base Function" << endl;
	}
};

class Derived :public Base {
public:
	void showDerived() {
		cout << "Derived Function" << endl;
	}
};

int main(void) {
	Derived d2;
	Derived *d1;
	Base *b = &d2;

	d1 = b;

	d1->showDerived();
	d1->showBase();
}